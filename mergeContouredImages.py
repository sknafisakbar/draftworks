import cv2
import numpy as np

# Load the three images with different color channels
image1 = cv2.imread('redNil.png')
image2 = cv2.imread('blueNil.png')
image3 = cv2.imread('greenNil.png')


sum_array = cv2.add(image1, cv2.add(image2, image3))

# Compute the mean
# mean_array = sum_array / 3

# Convert the numpy array back to an image format using OpenCV
mean_image = cv2.convertScaleAbs(sum_array)

# Save the resulting image
cv2.imwrite('mean_image_1.jpg', mean_image)






# Display the merged image
cv2.imshow('Merged Image', mean_image)
cv2.waitKey(0)
cv2.destroyAllWindows()
