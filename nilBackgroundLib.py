import cv2
import numpy as np

image_path ='./green.png'
image = cv2.imread(image_path,cv2.IMREAD_UNCHANGED)
    #image = image[200:600,400:700]
    #CONVERT IMAGE TO GRAYSCALE 
image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # onvert image to blck and white
th1, image_edges = cv2.threshold(image_gray, 150, 255, cv2.THRESH_BINARY) #th=>threshold
    #########
blur = cv2.GaussianBlur(image_gray,(5,5),0)
ret3,th3 = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    
    ########
th2 = cv2.Canny(image_gray,100,255)
    # create background mask
mask = np.zeros(image.shape, np.uint8)##
    # get most significant contours
contours_draw, hierachy = cv2.findContours(th3, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
x = cv2.drawContours(mask, contours_draw, -1, (255, 255, 255), -1)
good_image = cv2.bitwise_and(image, mask)##
    # cv2.imshow('sdewfer',x)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    #return good_image

#img = Nil_Background('./red.png')


cv2.imshow('blue_img', good_image)

while True:

    k=cv2.waitKey(0)
    if k==27:
        break
    
cv2.destroyAllWindows()
